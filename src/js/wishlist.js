$(document).ready(function(){

	clearBreadCrumb();
	addBreadCrumb("Lista de Deseos","./wishlist.html");
	loadBreadCrumb();

	var savedData=localStorage.getItem("accountData");
	if(savedData!=null){
		var ans=JSON.parse(savedData);
	}else{
		savedData=sessionStorage.getItem("accountData");
		if(savedData!=null){
			var ans=JSON.parse(savedData);
		}
	}

	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(result){

		
		if(result!=undefined && result.preferences!=null && result.preferences!=undefined)
			prefer=JSON.parse(result.preferences.replace( /\\/g ,''));
		if(result==undefined || result.preferences==undefined|| prefer.wishId==undefined || result.preferences==undefined){
			$(".not-logged-message").slideDown();
			$("#load-container").slideUp("slow");

		}else
		$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+preferences.wishId, success: function(res){
			if(res.order.items.length == 0){
				$("#addall").slideUp();
				$('#product_list').append('<div class="empty-cart">No tienes elementos en tu lista de deseos por el momento.</div>');
				$(".addall").remove();
				$("#load-container").fadeOut("slow");
				$("#product_list").delay(800).fadeIn("slow");
				$("#product_list").removeClass("tmp-container");
			}else{
				var itemCont=0;
				$.each(res.order.items, function(){
					$('#product_list_body').append(
						'<div class="row cart-row" id="product'+itemCont+'" name="id'+this.id+'"" value="prodid'+this.product.id+'" ">\
						<div class="col-md-4 cart-row-elem cart-img-col">\
						<a href="./productDescription.html?id='+this.product.id+'">\
						<img class="cart-row-img" src="'+this.product.imageUrl+'" alt="'+this.name+'">\
						</a>\
						</div>\
						<div class="col-md-3 cart-row-elem">'+this.product.name+'</div>\
						<div class="col-md-2 cart-row-elem align-right">$'+this.price+'</div>\
						<div class="col-md-2 cart-row-elem wish-to-cart-modal">\
						<button type="button" class="btn btn-success btn-lg wish-to-cart-btn" data-toggle="modal" data-target="#wishmodal'+itemCont+'"><img class="wish-to-cart-icon" src="./img/cart-icon.png" alt="Agregar al carrito"></button>\
						<div id="wishmodal'+itemCont+'" class="modal fade">\
						<div class="modal-dialog">\
						<div class="modal-content">\
						<div class="modal-header">\
						<button type="button" class="close" data-dismiss="modal">&times;</button>\
						<h4 class="modal-title">Agregar al carrito '+this.product.name+'</h4>\
						</div>\
						<div class="modal-body">\
						<div class="load-modal-container">\
						<img class="loading-img" src="./img/loading.gif" alt="Cargando contenido...">\
						Cargando campos...\
						</div>\
						<form class="wish-to-cart-form tmp-container" value="'+this.product.id+'">\
						<div>\
						<span class="required-field">* Campos Obligatorios</span>\
						</div>\
						<div class="form-group">\
						<label>Talle<span class="required-field">*</span></label>\
						<div>\
						<select id="sizeField'+itemCont+'" class="form-control">\
						\
						</select>\
						</div>\
						</div>\
						<div class="form-group">\
						<label>Color<span class="required-field">*</span></label>\
						<div>\
						<select id="colorField'+itemCont+'" class="form-control">\
						\
						</select>\
						</div>\
						</div>\
						<div class="form-group">\
						<label>Cantidad<span class="required-field">*</span></label>\
						<input id="quantField'+itemCont+'" type="number" min="1" value="1" class="form-control" name="cant">\
						</div>\
						</form>\
						</div>\
						<div class="modal-footer">\
						<button type="button" class="btn btn-success to-cart-btn" value="'+itemCont+'"  data-dismiss="modal">Agregar al carrito</button>\
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>\
						</div>\
						</div>\
						</div>\
						</div>\
						</div>\
						<div class="col-md-1 red-cross-elem">\
						<button id="dispose'+itemCont+'" type="button" class="disposeButton close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="remove_elem">&times;</span></button>\
						</div>\
						</div>'
						);
itemCont++;	
});
$("#product_list").slideDown();
$("#load-container").slideUp("slow");
}
}, complete:function(){
	$(".wish-to-cart-btn").click(function () {
		var num=$(this).attr('data-target').replace(/^#wishmodal/,'');
		var id=$("#product"+num).attr('value').replace(/^prodid/,'');
		$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductById&id="+id, success: function(apiprod){

			$.each(getValueFromJSON(apiprod.product.attributes,"Talle"),function(){
				$("#sizeField"+num).append("<option>"+this+"</option>");
			});

			$.each(getValueFromJSON(apiprod.product.attributes,"Color"),function(){
				$("#colorField"+num).append("<option>"+this+"</option>");
			});

			$(".load-modal-container").slideUp("slow");
			$(".wish-to-cart-form").delay(800).fadeIn("slow");
			$(".wish-to-cart-form").removeClass("tmp-container");

		}});
	});
	
	$(".to-cart-btn").click(function () {	
		var num=$(this).attr('value');
		var product= $("#product"+num);
		var quan=$("#quantField"+num).val();
		var apiProdId=$("#product"+num).attr('value').replace(/^prodid/,'')

		$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=AddItemToOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&order_item={"order":{"id":'+prefer.cartId+'},"product":{"id": '+apiProdId+'},"quantity":'+quan+'}', success: function(resp){
			var newElem={
				"color":$("#colorField"+num).val(),
				"size":$("#sizeField"+num).val(),
			};
			prefer["item"+resp.orderItem.id]=newElem;
			$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdatePreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&value='+JSON.stringify(prefer), success: function(newPref){
				$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=RemoveItemFromOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+product.attr('name').replace(/^id/,''), success: function(ans){
					product.slideUp();

					var prevCant= parseInt($("#badge-wish-logged").text());
					$("#badge-wish-logged").empty();
					$("#badge-wish-logged").append(prevCant-1);
					if(prevCant-1==0){
						$("#addAllModalTrigger").slideUp();
						$('#product_list').append('<div class="empty-cart">Todos los productos fueron agregados al carrito.</div>');
						$("#product_list").delay(800).fadeIn("slow");
						$("#product_list").removeClass("tmp-container");
					}
				}});
			}});
		}});
});


$(".disposeButton").click(function () {
	var product= $("#product"+this.id.replace(/^dispose/,''));
	$(this).slideUp();
	$(this).parent().append('<span class="textToAppear">Borrando...</span>');
	$(".textToAppear").slideDown();

	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=RemoveItemFromOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+product.attr('name').replace(/^id/,''), success: function(ans){
		product.slideUp();
		var prevCant= parseInt($("#badge-wish-logged").text());
		$("#badge-wish-logged").empty();
		$("#badge-wish-logged").append(prevCant-1);

		if(prevCant==1){
			$(".total-container").slideUp();
			$('#product_list').append('<div class="empty-cart">No tienes elementos en tu carrito por el momento.</div>');
			$("#product_list").delay(800).fadeIn("slow");
			$("#addAllModalTrigger").slideUp();
		}

	}});
});


}});
}});

$(".addall").click(function () {
	if($("#add-all-body-content").is(':empty')){
		$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(result){
			prefer=JSON.parse(result.preferences.replace( /\\/g ,''));
			var itemCont=0;
			$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+preferences.wishId, success: function(res){
				$.each(res.order.items, function(){
					$("#add-all-body-content").append('<div class="add-all-node">\
						<div class="add-all-title">\
						<label>'+this.product.name+'</label>\
						</div>\
						<div class="form-group add-all-field">\
						<label>Talle<span class="required-field">*</span></label>\
						<div>\
						<select id="allSizeField'+itemCont+'" class="form-control">\
						</select>\
						</div>\
						</div>\
						<div class="form-group add-all-field">\
						<label>Color<span class="required-field">*</span></label>\
						<div>\
						<select id="allColorField'+itemCont+'" class="form-control">\
						</select>\
						</div>\
						</div>\
						<div class="form-group add-all-field">\
						<label>Cantidad<span class="required-field">*</span></label>\
						<input id="allQuantField'+itemCont+'" type="number" min="1" value="1" class="form-control" name="cant">\
						</div>\
						<hr class="add-all-line">\
						</div>');
					fillContent(itemCont);
					itemCont++;	
				});
$("#load-all-container").slideUp("slow");
$("#add-all-body").delay(800).fadeIn("slow");
$("#add-all-body").removeClass("tmp-container");
},complete:function(){

	$("#add-all-submit-button").click(function () {

		$(this).prop('disabled',true);
		$(this).empty();
		$(this).append("Agregando productos al carrito...");


		for(var counter=0;counter<itemCont;counter++){
			var num=counter;
			var product= $("#product"+num);
			var quan=$("#allQuantField"+num).val();
			var apiProdId=$("#product"+num).attr('value').replace(/^prodid/,'');

			disposeAll(ans,quan,apiProdId,num,product,counter==itemCont-1);
		}});
}});
}});
}});


function fillContent(num){
	var id=$("#product"+num).attr('value').replace(/^prodid/,'');
	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductById&id="+id, success: function(apiprod){

		$.each(getValueFromJSON(apiprod.product.attributes,"Talle"),function(){
			$("#allSizeField"+num).append("<option>"+this+"</option>");
		});

		$.each(getValueFromJSON(apiprod.product.attributes,"Color"),function(){
			$("#allColorField"+num).append("<option>"+this+"</option>");
		});
	}});
}
});

function disposeAll(ans,quan,apiProdId,num,product,last){
	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(result){
		prefer=JSON.parse(result.preferences.replace( /\\/g ,''));
		$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=AddItemToOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&order_item={"order":{"id":'+prefer.cartId+'},"product":{"id": '+apiProdId+'},"quantity":'+quan+'}', success: function(resp){
			var newElem={
				"color":$("#allColorField"+num).val(),
				"size":$("#allSizeField"+num).val(),
			};
			prefer["item"+resp.orderItem.id]=newElem;
			$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdatePreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&value='+JSON.stringify(prefer), success: function(newPref){
				$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=RemoveItemFromOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+product.attr('name').replace(/^id/,''), success: function(ans){
					product.slideUp();
					if(last){
						$('#addAllModal').modal('toggle');
						$('#addAllModalTrigger').slideUp();
						$("#addall").slideUp();
						$('#product_list').append('<div class="empty-cart">Todos los productos se agregaron al carrito.</div>');
						$(".addall").remove();
						$("#load-container").fadeOut("slow");
						$("#product_list").delay(800).fadeIn("slow");
						$("#product_list").removeClass("tmp-container");


						var prevCant= parseInt($("#badge-wish-logged").text());
						var currectCart=parseInt($("#badge-cart-logged").text());
						$("#badge-wish-logged").empty();
						$("#badge-wish-logged").append(0);
						$("#badge-cart-logged").empty();
						$("#badge-cart-logged").append(prevCant+currectCart);
					}
				}});
}});
}});
}});
}