var savedData; //Datos del login
var ans;// Preferencias totales
var preferences; // Preferencias temporales del usuario
var tmp; //Preferencias de items de producto
var total_input; // Objeto para almacenar respuestas
var old_cart_content; //Contenido del carrito estatico del usuario
var address; //Direccion de envio de la compra

$(document).ready(function() {

	clearBreadCrumb();
	addBreadCrumb("Comprar","./buy.html");
	loadBreadCrumb();

	total_input={};

	savedData=localStorage.getItem("accountData");
	if(savedData!=null){
		ans=JSON.parse(savedData);
	}else{
		savedData=sessionStorage.getItem("accountData");
		if(savedData!=null){
			ans=JSON.parse(savedData);
		}
	}
	forkedCart=null;
	preferences=ans;

	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(result){
		tmp=preferences=JSON.parse(result.preferences.replace( /\\/g ,''));
		loadFirstItem();
	}});
	

	$('.go-to-cart').click(function(){
		goToCart();
	});

	$('.go-to-shipping').click(function(){
		goToShipping();
		loadSecondItem();
	});

	$('.go-to-payment').click(function(){
		loadAddressData();
		goToPayment();
		loadThirdItem();
	});

	$('.go-to-confirmation').click(function(){
		loadPaymentData();
	});


	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Common.groovy?method=GetAllStates", success: function(result){
		if(result.error==undefined){
			$.each(result.states, function(){
				$('#provincia').append('<option value="' + this.stateId + '">' + this.name +'</option>');	
			});
			$( ".total-element" ).unbind( "click");
		}else{
			displayApiError(result.error.code,result.error.message);
		}
	}});
});


function loadFirstItem(){
	total_input.cartProducts=[];
	total_input.removedProdId=[];
	var total_price=0;





	
	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+tmp.cartId, success: function(res){
		if(res.error==undefined){
			if(res.order.items.length == 0){
				$(".total-container").slideUp();
				$('#product_list').append('<div class="empty-cart">No tienes elementos en tu carrito por el momento.</div>');
				$("#load-container").fadeOut("slow");
				$("#product_list").delay(800).fadeIn("slow");
				$("#product_list").removeClass("tmp-container");
			}else{
				var itemCont=0;
				$.each(res.order.items, function(){
					var pref_color="-";
					var pref_size="-";

					newProdPref=tmp["item"+this.id];

					if(newProdPref!=undefined){
						pref_color=( newProdPref.color == undefined)?"-":newProdPref.color;
						pref_size=( newProdPref.size == undefined)?"-":newProdPref.size;
					}

					total_price+=this.price*this.quantity;
					$('#product_list').append(
						'<div class="row cart-row" id="product'+itemCont+'" name="id'+this.id+'" ">\
						<div class="col-md-3">'+this.product.name+'</div>\
						<div class="col-md-1">'+pref_color+'</div>\
						<div class="col-md-1">'+pref_size+'</div>\
						<div class="col-md-2 align-right">'+this.quantity+'</div>\
						<div class="col-md-2 align-right">$'+this.price+'</div>\
						<div class="col-md-2 align-right" id="subt'+itemCont+'">$'+(this.price*this.quantity)+'</div>\
						<div class="col-md-1">\
						<button id="dispose'+itemCont+'" type="button" class="disposeButton close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="remove_elem">&times;</span></button>\
						</div>\
						</div>\
						<hr class="add-all-line" id="hr'+itemCont+'">'
						);
					total_input.cartProducts.push({"name":this.product.name,"price":this.price,"quantity":this.quantity,"id":this.id});
					itemCont++;
				});
				$('#total_price').append('$'+total_price);
				$("#load-container").fadeOut("slow");
				$("#product_list").delay(800).fadeIn("slow");
				$("#product_list").removeClass("tmp-container");
			}	
		}else{		
			displayApiError(res.error.code,res.error.message);
		}
}, complete:function(){
	$(".disposeButton").click(function () {
		num=this.id.replace(/^dispose/,'');
		var product= $("#product"+num);
		$("#hr"+num).slideUp();

		total_input.removedProdId.push(parseInt(product.attr('name').replace(/^id/,'')));
		product.slideUp();

		$('#total_price').fadeOut();
		$('#total_price').empty();
		total_price-=parseInt($("#subt"+num).text().split("$")[1]);
		$('#total_price').append('Precio Total: $'+total_price);
		$('#total_price').fadeIn();
	});
}});
}


function loadSecondItem(){
	total_input.address = {};
	address = {};

	//Deshabilito los botones de seguir
	//$(".btn btn-success go-to-payment").prop('disabled', true);

	//Se vuelven a habilitar cuando se selecciona una direccion existente o cuando
	//se completan los campos y valida

	//Cacheo cuando selecciona "Seguir" o "Guardar y seguir" (llaman a la misma funcion)

	var request = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllAddresses';
	request += '&username=' + ans.account.username;
	request += '&authentication_token=' + ans.authenticationToken;

	//Para cargar address
	$.ajax({url: request, success: function(result){
		if(result.error!=undefined){
			alert("ERROR CODE: "+ result.error.code);
		}
		$.each(result.addresses, function(){
			$('#direcciones').append('<option id="' + this.id + '">' + this.name +'</option>');
			sessionStorage.setItem("address-id-" + this.id, JSON.stringify(this));
		});
	}});

	$( "#direcciones" ).change(function(){
		if( $(this).val() != '-' ){
			completeForms($( "#direcciones option:selected" ).attr("id"));
			blockForms(true);
		} else {
			cleanForms();
			blockForms(false);
		}
	});
}

function loadAddressData(){
	if( $("#direcciones").val() == '-' ){
		address.name = $("#nombreDir").val();
		address.province = $("#provincia").val();
		address.city = $("#ciudad").val();
		address.street = $("#calle").val();
		address.number = $("#numero").val();
		address.floor = $("#dpto").val();
		address.zipCode = $("#codPost").val();
		address.phoneNumber = $("#numeroTel").val();		
	}
	total_input.address = address;
}


function completeForms(addressId){
	address = JSON.parse(sessionStorage.getItem("address-id-" + addressId));
	$("#nombreDir").val(address.name);
	$("#provincia").val(address.province);
	$("#ciudad").val(address.city);
	$("#calle").val(address.street);
	$("#numero").val(address.number);
	$("#dpto").val(address.floor);
	$("#codPost").val(address.zipCode);
	$("#numeroTel").val(address.phoneNumber);
}

function blockForms(bool){
	$("#nombreDir").prop('disabled', bool);
	$("#provincia").prop('disabled', bool);
	$("#ciudad").prop('disabled', bool);
	$("#calle").prop('disabled', bool);
	$("#numero").prop('disabled', bool);
	$("#dpto").prop('disabled', bool);
	$("#codPost").prop('disabled', bool);
	$("#numeroTel").prop('disabled', bool);
}

function cleanForms(){
	$("#nombreDir").val("");
	$("#provincia").val("");
	$("#ciudad").val("");
	$("#calle").val("");
	$("#numero").val("");
	$("#dpto").val("");
	$("#codPost").val("");
	$("#numeroTel").val("");
}


function loadThirdItem(){
	total_input.paymentType = {};
	total_input.creditCardDetails = {};

	$( ".paymentType" ).change(function(){
		if( $(this).val() == "TarjetaCredito"){
			cleanForms();
			$(".card-details").slideDown();
		} else {
			$(".card-details").slideUp();
		}
	});
}

function loadPaymentData(){
	if( $(".paymentType").val() == "TarjetaCredito"){
		total_input.paymentType = "Tarjeta de Crédito";
		var request = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=CreateCreditCard';
		var months = $(".months-buy").val() < 10 ? "0" + $(".months-buy").val() : $(".months-buy").val();

		request += '&username=' + ans.account.username;
		request += '&authentication_token=' + ans.authenticationToken;
		request += '&credit_card={"number":"' + $(".cardNumber").val() + '",';
		request += '"expirationDate":"' + months + $(".years-buy").val().split("20")[1] + '",';
		request += '"securityCode":"' + $(".cardCode").val() + '"}';

		$.when($.ajax({url: request, success: function(result){
			if(result.error!=undefined){
				alert("ERROR CODE: "+ result.error.code);
			}
			total_input.creditCardDetails = result.creditCard;
		}})).then( function(){
			goToConfirmation();
			loadFourthItem();
		});

	} else {
		total_input.paymentType = "Contra reembolso";
		goToConfirmation();
		loadFourthItem();
	}
}

function loadFourthItem(){

	/* Detalles de pago */
	loadPaymentDetails();
	
	/* Detalles de productos */
	loadProductsDetails();

	/* Detalles de envio */
	loadAddressDetails();

	$("#order-confirm-btn").click(function () {
		$("#order-confirm-btn").empty();
		$(".btn").prop('disabled',true);
		$("#order-confirm-btn").append("Confirmando pedido...");
		
		if(total_input.removedProdId.length==0){
			forkCartId();
		}else{
			$.each(total_input.removedProdId , function( index, value ) {
				$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=RemoveItemFromOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+value, success: function(result){
					if(index==total_input.removedProdId.length-1){
						forkCartId();
					}
				}});
			});
		}
	});
}


function forkCartId(){
	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=CreateOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(newFixCart){
		var newCartId=newFixCart.order.id;

		var request="";
		if(total_input.creditCardDetails!=undefined && total_input.creditCardDetails.id!=undefined){
			request="http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=ConfirmOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&order={"id":'+tmp.cartId+',"address":{"id":'+total_input.address.id+'},"creditCard":{"id":'+total_input.creditCardDetails.id+'}}';
		}else{
			request="http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=ConfirmOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&order={"id":'+tmp.cartId+',"address":{"id":'+total_input.address.id+'}}';
		}

		$.ajax({url: request, success: function(confirmed){
			tmp.cartId=newCartId;
			$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdatePreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&value="+JSON.stringify(tmp), success: function(confirmed){

				$(".step-wizard-container").slideUp();
				$(".btn").prop('disabled',false);
				$("#badge-cart-logged").empty();
				$("#badge-cart-logged").append(0);
				$(".item.active").empty();
				$(".step-wizard-container").empty();
				$(".step-wizard-container").append('<div class="finish-text">Su pedido se ha confirmado correctamente.</div>');
				$(".step-wizard-container").slideDown();
			}});
		}});
	}});
}

function loadAddressDetails(){
	$(".addressDetails").append('<p>Nombre: ' + total_input.address.name + '</p>\
		<p>Dirección: ' + total_input.address.street + ', ' + total_input.address.city + '</p>\
		<p>Código Postal: ' + total_input.address.zipCode + '\t Número de teléfono: \
		' + total_input.address.phoneNumber +'</p>');
}


function loadPaymentDetails(){
	var paymentDetails;

	if( total_input.paymentType == "Contra reembolso" ){
		paymentDetails = '<p>Forma de pago: ' + total_input.paymentType + '</p>';
	} else {
		paymentDetails = '<p>Forma de pago: ' + total_input.paymentType + '</p>\
		<p>Número de tarjeta: ' + total_input.creditCardDetails.number + '</p>\
		<p>Fecha de vencimiento: ' + total_input.creditCardDetails.expirationDate + '</p>\
		<p>Código de seguridad: ' + total_input.creditCardDetails.securityCode + '</p>';
	}
	$(".paymentDetails").append(paymentDetails);
}


function loadProductsDetails(){
	var product, total=0;
	for(var i=0; i<total_input.cartProducts.length; i++){
		product = total_input.cartProducts[i];
		if(	$.inArray(product.id, total_input.removedProdId) == -1){
			total += product.quantity * product.price;
			$("#product_list2").append('<div class="row">\
				<div class="col-md-5">' + product.name + '</div>\
				<div class="col-md-2 align-right">$' + product.price+'</div>\
				<div class="col-md-3 align-right">' + product.quantity+'</div>\
				<div class="col-md-2 align-right">$' + product.quantity * product.price +'</div>\
				</div>\
				<hr class="add-all-line">');
		}
	}
	$("#load-container2").fadeOut("slow");
	$(".total_price2").append(total);
}

function goToCart(){
	$('#shipping-mark').removeClass("btn-success");
	$('#shipping-mark').addClass("btn-default");
	$('#payment-mark').removeClass("btn-success");
	$('#payment-mark').addClass("btn-default");
	$('#confirmation-mark').removeClass("btn-success");
	$('#confirmation-mark').addClass("btn-default");
	$('.carousel').carousel(0);
}

function goToShipping(){
	$('#shipping-mark').removeClass("btn-default");
	$('#shipping-mark').addClass("btn-success");

	$('#payment-mark').removeClass("btn-success");
	$('#payment-mark').addClass("btn-default");
	$('#confirmation-mark').removeClass("btn-success");
	$('#confirmation-mark').addClass("btn-default");

	$('.carousel').carousel(1);
}
function goToPayment(){
	$('#shipping-mark').removeClass("btn-default");
	$('#shipping-mark').addClass("btn-success");
	$('#payment-mark').removeClass("btn-default");
	$('#payment-mark').addClass("btn-success");

	$('#confirmation-mark').removeClass("btn-success");
	$('#confirmation-mark').addClass("btn-default");

	$('.carousel').carousel(2);
}
function goToConfirmation(){
	$('#shipping-mark').removeClass("btn-default");
	$('#shipping-mark').addClass("btn-success");
	$('#payment-mark').removeClass("btn-default");
	$('#payment-mark').addClass("btn-success");
	$('#confirmation-mark').removeClass("btn-default");
	$('#confirmation-mark').addClass("btn-success");
	$('.carousel').carousel(3);
}