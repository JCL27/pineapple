var savedData
$(document).ready(function(){
	
	savedData=JSON.parse(localStorage.getItem("accountData"));
	if(savedData != null && savedData != undefined && savedData.error != undefined){
		localStorage.removeItem("accountData");
	}
	if(savedData==null || savedData == undefined)

		if(savedData != null && savedData != undefined && savedData.error != undefined){
			sessionStorage.removeItem("accountData");
		}

		$(".toCart").click(function () {
			location.href = "./cart.html";
		});

		$("#search-form").submit(function(){
			if(typeof first === "undefined"){
				location.href = './searchResult.html?key=' + $('#search-input').val();
			}
			return false;
		});

		$(".toWish").click(function () {
			location.href = "./wishlist.html";
		});

		$(".toConstruct").click(function () {
			location.href = "./inConstruction.html";
		});

		$(".toFootwear").click(function () {
			clearBreadCrumb();
			addBreadCrumb("Calzado","./searchResult.html?key=Calzado");
			location.href = "./searchResult.html?key=Calzado";
		});

		$(".toClothing").click(function () {
			clearBreadCrumb();
			addBreadCrumb("Ropa","./searchResult.html?key=Indumentaria");
			location.href = "./searchResult.html?key=Indumentaria";
		});

		$(".toAccesories").click(function () {
			clearBreadCrumb();
			addBreadCrumb("Accesorios","./searchResult.html?key=Accesorios");
			location.href = "./searchResult.html?key=Accesorios";
		});

		$(".toSale").click(function () {
			clearBreadCrumb();
			addBreadCrumb("Promociones","./searchResult.html?key=&filter=Oferta&value=Oferta");
			location.href = "./searchResult.html?key=&filter=Oferta&value=Oferta";
		});

		$(".toMen").click(function () {
			clearBreadCrumb();
			addBreadCrumb("Hombre","./category.html?id=5");
			location.href = "./category.html?id=5";
		});

		$(".toWomen").click(function () {
			clearBreadCrumb();
			addBreadCrumb("Mujer","./category.html?id=6");
			location.href = "./category.html?id=6";
		});

		$(".toKids").click(function () {
			clearBreadCrumb();
			addBreadCrumb("Niño","./category.html?id=7");
			location.href = "./category.html?id=7";
		});

		$( ".dropdown").hover(
			function() {
				$( this ).addClass("open");
				$( this ).children().attr("aria-expanded","true");
			}, function() {
				$( this ).removeClass("open");
				$( this ).children().attr("aria-expanded","false");
			}
			);

		searchLoggedSesion();

		initializeSubcategories();

		$("#login-btn").click(function(){

			sessionStorage.removeItem("accountData");
			localStorage.removeItem("accountData");

			var uname = $('#uName').val();
			var upass = $('#uPassword').val();

			login(uname,upass,false);

		});


		$("#logoff").click(function(){
			$("#logged-static-bar").removeClass("dinamic-navbar-component-active");
			$("#unlogged-static-bar").addClass("dinamic-navbar-component-active");	

			$("#greeting-text").empty();
			$('#uName').val("");
			$('#uPassword').val("");

			sessionStorage.removeItem("accountData");
			localStorage.removeItem("accountData");
			if(window.location.href.indexOf("userConfig") > -1){
				window.location.href = "./mainPage.html";
			}
		});

		$("#register-button").click(function(){
			

			$("#reg-message").slideUp();
			$("#reg-message").empty();
			$("#register-button").prop('disabled',true);
			$("#register-button").empty();
			$("#register-button").append("Registrando...");

			var regis = {};

			regis.name = $('#name').val();
			regis.surname = $('#surname').val();
			regis.dni = $('#dni').val();
			regis.email = $('#email1').val();
			regis.username = $('#username').val();
			regis.gender = $('#gender').val();
			regis.pwdreg = $('#pwdreg').val();
			regis.checkpwd = $('#checkpwd').val();
			regis.birthDate = $('#registry-years').val() + '-';
			if($('#registry-months').val()<10)
				regis.birthDate += "0";
			regis.birthDate += $('#registry-months').val() + '-';
			if($('#registry-days').val()<10)
				regis.birthDate += "0";
			regis.birthDate += $('#registry-days').val();


			if(regis.username==""){

				regis.username=hashMail(regis.email);
			}

			var request = "http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=CreateAccount&account={";

			request += '"username":"' + regis.username + '",';
			request += '"password":"' + regis.pwdreg + '",';
			request += '"firstName":"' + regis.name + '",';
			request += '"lastName":"'+ regis.surname + '",';
			request += '"gender":"' + regis.gender + '",';
			request += '"identityCard":"' + regis.dni + '",';
			request += '"email":"'+ regis.email + '",';
			request += '"birthDate":"' + regis.birthDate + '"';
			request += "}";

			$.ajax({url: request, success: function(result){
				if(result.error!=undefined){
					switch(result.error.code){
						case 103:
						$("#reg-message").append("Los datos de la cuenta de usuarios no son válidos");
						break;
						case 104:
						$("#reg-message").append("El nombre de usuario no es válido");
						break;
						case 105:
						$("#reg-message").append("El formato de la contraseña  no es válido");
						break;
						case 106:
						$("#reg-message").append("El nombre no es válido");
						break;
						case 109:
						$("#reg-message").append("El DNI no es válido");
						break;
						case 110:
						$("#reg-message").append("El email no es válido");
						break;
						case 111:
						$("#reg-message").append("La fecha de nacimiento no es válida");
						break;
						case 200:
						$("#reg-message").append("El nombre de usuario ya existe");
						break;
						case 201:
						$("#reg-message").append("El DNI ya existe");
						break;
						default:
						$("#reg-message").append("Error desconocido. Intente nuevamente más tarde");
						break;
					}
					$("#reg-message").slideDown();
					$("#register-button").prop('disabled', false);
					$("#register-button").empty();
					$("#register-button").append("Ingresar");
				}else{
					login(regis.username,regis.pwdreg,true);
				}
			}});
});
});



function hashMail(email){
	var hash = "0";
	var email_length = email.length;
	var hash_size = 3;
	var acu;

	for(var i = 0; i < email_length; i+=hash_size){
		acu = 0;
		for(var j = 0; j < hash_size && (j+i) < email_length; j++){
			acu+=email.charCodeAt(i+j);
		}
		acu = acu % 62;
		if(acu<10){
			acu = String.fromCharCode(acu + "0".charCodeAt(0));
		}else if (acu < 36){
			acu = String.fromCharCode(acu - 10 + "A".charCodeAt(0));
		}else{
			acu = String.fromCharCode(acu - 36 + "a".charCodeAt(0));
		}
		hash += acu;
	}
	return hash;
}


function changeBarStatus(ans){
	if(ans!=null){
		$("#unlogged-static-bar").removeClass("dinamic-navbar-component-active");
		$("#logged-static-bar").addClass("dinamic-navbar-component-active");	
		var gretEnd=(ans.account.gender=="M"?"o":"a");

		$("#greeting-text").append("¡Bienvenid"+gretEnd+" "+ans.account.firstName+"!");


		$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(res){
			if(res.error==undefined){
				if(res!=undefined && res.preferences!=undefined){
					preferences=JSON.parse(res.preferences.replace( /\\/g ,''));
					$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+preferences.cartId, success: function(cart){
						$('#badge-cart-logged').empty();
						$('#badge-cart-logged').append(cart.order.items.length);
					}});

					$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+preferences.wishId, success: function(wish){
						$('#badge-wish-logged').empty();
						$('#badge-wish-logged').append(wish.order.items.length);
					}});
				}
			}else{
				displayApiError(res.error.code,res.error.message);
			}
		}});
}
}

function login(uname,upass,createCarts){

	if(uname.indexOf("@")>-1){
		uname=hashMail(uname);
	}
	if(!createCarts){
		$("#state-message").slideUp();
		$("#state-message").empty();
		$("#login-btn").prop('disabled', true);
	}
	
	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=SignIn&username="+uname+"&password="+upass, success: function(ans){
		if(ans.error!=undefined){
			switch(ans.error.code){
				case 999:$("#state-message").append("Error desconocido. Intente nuevamente");
				break;
				default:
				$("#state-message").append("El usuario o la contraseña no son válidos");
			}
			$("#state-message").slideDown();
			$("#login-btn").prop('disabled', false);
		}else{
			if($("#keepMe").is(':checked')){
				localStorage.setItem("accountData",JSON.stringify(ans));
			}else{
				sessionStorage.setItem("accountData",JSON.stringify(ans));	
			}
			savedData = ans;
			if(ans.error == undefined){
				if(createCarts){
					var cartId,wishId;
					$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=CreateOrder&username="+savedData.account.username+"&authentication_token="+savedData.authenticationToken, success: function(result){
						cartId=result.order.id;
						$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=CreateOrder&username="+savedData.account.username+"&authentication_token="+savedData.authenticationToken, success: function(result2){
							wishId=result2.order.id
							$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdatePreferences&username="+savedData.account.username+"&authentication_token="+savedData.authenticationToken+'&value={"cartId":'+cartId+',"wishId":'+wishId+'}', success: function(ans){
								$("#login-btn").prop('disabled', false);
								$("#register-button").prop('disabled',false);
								$('#signUpModal').modal('toggle');
								$(".form-control").empty();
								$("#register-button").empty();
								$("#register-button").append("Ingresar");
							}});
						}});
					}});
				}else{
					$('#logInModal').modal('toggle');
					$('#signUpModal').modal('toggle');
					$("#login-btn").prop('disabled', false);
					$("#register-button").prop('disabled',false);
					$("#register-button").empty();
					$("#register-button").append("Ingresar");
				}
				changeBarStatus(savedData);
			}
		}
	}});
}

function searchLoggedSesion(){
	savedData=localStorage.getItem("accountData");
	if(savedData!=null){
		var ans=JSON.parse(savedData);
	}else{
		savedData=sessionStorage.getItem("accountData");
		if(savedData!=null){
			var ans=JSON.parse(savedData);
		}
	}
	changeBarStatus(ans);
}

$(window).resize(function(){
	if ($(window).width() >= 768){  
		$('#categories-bar').addClass('in');
		$('#search-navbar').removeClass('in');


		$('#follow-us').addClass('row');
		$('.footer-second-level-inner').addClass('row');
		$('#footer-second-level').addClass('row');
	}

	if ($(window).width() <= 768){

		$('#categories-bar').removeClass('in');
		$('#search-navbar').addClass('in');

		$('#follow-us').removeClass('row');
		$('.footer-second-level-inner').removeClass('row');
		$('#footer-second-level').removeClass('row');

	}
});



function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	sURLVariables = sPageURL.split('&'),
	sParameterName,
	i;
	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];

		}
	}
}

// Recive el vector de atributos, ni más ni menos. Retorna un vector de values o ["-"] si no lo encontró
function getValueFromJSON(obj,keyName){
	keyName=keyName.toUpperCase();
	var final_ans=["-"];
	$.each(obj,function(){
		var atName=this.name.toUpperCase();
		if(keyName.indexOf(atName)==0 || atName.indexOf(keyName)==0 ){
			final_ans=this.values;
		}
	});
	return final_ans;
}



function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	sURLVariables = sPageURL.split('&'),
	sParameterName,
	i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
	return -1;
};


function getFromUrlParameter(sPageURL, sParam) {
	sURLVariables = sPageURL.split('&'),
	sParameterName,
	i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
	return -1;
};


// Recive el vector de atributos, ni más ni menos. Retorna un vector de values o ["-"] si no lo encontró
function getValueFromJSON(obj,keyName){
	keyName=keyName.toUpperCase();
	var final_ans=["-"];
	$.each(obj,function(){
		var atName=this.name.toUpperCase();
		if(keyName.indexOf(atName)==0 || atName.indexOf(keyName)==0 ){
			final_ans=this.values;
		}
	});
	return final_ans;
}

function imgError(image) {
	image.onerror = "";
	image.src = "./img/noimage.png";
}

function imgDeteleError(image) {
	image.closest("li").remove();
}

function initializeSubcategories(){

	var categories = [];

	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllCategories", success: function(result){
		$.each(result.categories, function(){
			categories.push(this.name);
		});

		var cont = 0;
		$.each(categories,function(){

			cont++;
			$('.toMen > ul').append(createCategoryNode( this, cont,"Genero","Hombre"));
			$('.toWomen> ul').append(createCategoryNode( this, cont,"Genero","Mujer"));
			$('.toKids> ul').append(createCategoryNode( this, cont,"Edad","Bebe"));
			
		});
		
		var cont2=1;
		while(cont2 < 4){
			loadSubCategories(cont2);
			cont2++;
		}

	}});
}

function loadSubCategories(cont2){
	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllSubcategories&id="+cont2+"&filters=[{%22id%22:1,%22value%22:%22Masculino%22}]", success: function(result){
		categories=[];
		$.each(result.subcategories, function(){
			categories.push(this.name);
		});
		var cont3=0;
		$.each(categories,function(){
			var node='<li><a href="./searchResult.html?key='+this+'&filter=Genero&value=Masculino">'+this+'</a></li>';
			$('.toMen>ul #cat'+cont2+' ul').append(node);
			cont3++;
		});
		$('.toMen>ul #cat'+cont2).not(":has(li)").remove();


		/*if(cont2==2){
			$(".dropdown-submenu .dropdown-menu li a").click(function(){
				createRuntimeBC(this);
			});
}*/
}});

	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllSubcategories&id="+cont2+"&filters=[{%22id%22:1,%22value%22:%22Femenino%22}]", success: function(result){
		categories=[];
		$.each(result.subcategories, function(){
			categories.push(this.name);
		});
		var cont4=0;
		$.each(categories,function(){
			var node='<li><a href="./searchResult.html?key='+this+'&filter=Genero&value=Femenino">'+this+'</a></li>';
			$('.toWomen>ul #cat'+cont2+' ul').append(node);
			cont4++;
		});
		$('.toWomen>ul #cat'+cont2).not(":has(li)").remove();

		/*if(cont2==2){
			$(".dropdown-submenu .dropdown-menu li a").click(function(){
				createRuntimeBC(this);
			});
}*/
}});

	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllSubcategories&id="+cont2+"&filters=[{%22id%22:%202,%22value%22:%22Bebe%22}]", success: function(result){
		categories=[];
		$.each(result.subcategories, function(){
			categories.push(this.name);
		});
		var cont5=0;
		$.each(categories,function(){
			var node='<li><a href="./searchResult.html?key='+this+'&filter=Edad&value=Infantil">'+this+'</a></li>';
			$('.toKids>ul #cat'+cont2+' ul').append(node);
			cont5++;
		});

		$('.toKids>ul #cat'+cont2).not(":has(li)").remove();	

		/*if(cont2==2){
			$(".dropdown-submenu .dropdown-menu li a").click(function(){
				createRuntimeBC(this);
			});
}*/
}});
}



function createCategoryNode(name,pos){
	/*var node =
	'<li id="cat'+pos+'"><a href=\"./category.html\">'+name+'</a></li>';
	return node;
	*/

	var node=
	'<li id="cat'+pos+'" class="dropdown-submenu complete-width">\
	<a tabindex="-1" href="#">'+name+'</a>\
	<ul class="dropdown-menu">\
	</ul>\
	</li>';

	return node;
}

function createRuntimeBC(element){
	
	/* COMENTARIOS PARA MARCO
	* 1) Los breadcrumbs los guardo en sessionStorage con el formato:
	*   --->   [{name:"...",url:"..."}]
	* 2) El metodo que los appendea es: (esta al final del archivo)
	*   --->   addBreadCrumb(...)
	* 3) El metodo que los carga (una vez que entra en la pagina) es:
	*   --->   loadBreadCrumb
	*/

	/* Current */

	alert("ELEMENTO: " + (element));

	var value1 = getFromUrlParameter($(element).attr("href"), "key");
	var locationRef1 = $(element).attr("href");


	/* Father */
	var value2 = $(element).parent().parent().parent().parent().prev().val();
	var locationRef2 = $(element).parent().parent().parent().parent().prev().attr("href"); //TODO: CAMBIAR!!

	/* From left to right */
	var breadcrumb = [ {name:value2,url:locationRef2} , {name:value1,url:locationRef1}  ];
	addBreadCrumb(breadcrumb);
}

function clearBreadCrumb(){
	sessionStorage.removeItem("breadcrumbs");
	$(".breadcrumb").empty();
}

function loadBreadCrumb(){
	


	var breadObj=getBreadcrumbsObj();

	$.each(breadObj, function( index, value ) {
		if(index==breadObj.length-1){
			$(".breadcrumb").append('<li class="active">'+value.title+'</li>');
		}else{
			$(".breadcrumb").append('<li><a href="'+value.bcUrl+'">'+value.title+'</a></li>');
		}

	});
}


function addBreadCrumb(title,url){
	var breadObj=getBreadcrumbsObj();

	breadObj.push({"title":title,"bcUrl":url});	
	sessionStorage.setItem("breadcrumbs", JSON.stringify(breadObj));
}

function isEmptyBreadCrumb(){
	return 	$(".breadcrumb").text()=="";
}

function getBreadcrumbsObj(){
	var storageItem=sessionStorage.getItem("breadcrumbs");
	if(storageItem==null){
		storageItem=[];
		storageItem.push({"title":"Home","bcUrl":"./mainPage.html"});	
		sessionStorage.setItem("breadcrumbs",JSON.stringify(storageItem));
	}
	storageItem=sessionStorage.getItem("breadcrumbs");
	return JSON.parse(storageItem);
}

function displayApiError(message,code){
	$('.modal').modal('hide');
	$('.static-top-bar').append(
		'<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModal">\
		<div class="modal-dialog" role="document">\
		<div class="modal-content">\
		<div class="modal-header">\
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>\
		<h4 class="modal-title" id="myModalLabel">Oh oh, ocurrió algo inesperado</h4>\
		</div> \
		<form name="signInForm" ng-controller ="signinvalidator">\
		<div class="modal-body center-text">\
		<img class="api-error-image" src="./img/error.jpg" alt="Imagen de error"> \
		<p class="error-body">Lo sentimos, ha ocurrido un problema con el servidor.\
		<br>Por favor, intente ingresar nuevamente más tarde.</p>\
		<p class="error-sign"> ¡Disculpe las molestias!<br> PineApple</p>\
		</div>\
		<div class="panel-group" id="accordion">\
		<div class="panel panel-default" id="panel1">\
		<div class="panel-heading">\
		<h4 class="panel-title">\
		<a data-toggle="collapse" data-target="#collapseOne" href="#collapseOne">\
		Detalles\
		</a>\
		</h4>\
		</div>\
		<div id="collapseOne" class="panel-collapse collapse">\
		<div class="panel-body">\
		Codigo de error: '+code+'<br>\
		Mensaje de error: '+message+'<br>\
		</div>\
		</div>\
		</div>\
		</div>\
		</form>\
		</div>\
		</div>\
		</div>'
		);
$('#errorModal').modal('show');
}

