$(document).ready(function(){

	clearBreadCrumb();
	loadBreadCrumb();

	loadMainPageImages();


});


function loadMainPageImages(){

	var savedData=localStorage.getItem("accountData");
	var ans;
	if(savedData!=null){
		ans=JSON.parse(savedData);
	}else{
		savedData=sessionStorage.getItem("accountData");
		if(savedData!=null){
			ans=JSON.parse(savedData);
		}
	}
	if(savedData==null){
		$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&filters=[{%22id%22:5,%22value%22:%22Oferta%22}]&page_size=100", success: function(result){
			shuffle(result.products);
			var i=0;
			while (i<3){
				var prod=result.products[i];
				$('#saleCont').append('<div class="saleDiv">\
					<div class="saleName">'+getRandomSaleText()+'</div>\
					<a href=./productDescription.html?id='+prod.id+'>\
					<img class="mySale" src="'+prod.imageUrl[0]+'" alt="Oferta" onerror="imgError(this);">\
					</a>\
					<p class="saleName">'+prod.name+' - $'+prod.price+'</p>\
					</div>');
				i++;
			}		
		}});

		$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&filters=[{%22id%22:6,%22value%22:%22Nuevo%22}]&page_size=100", success: function(result){
			shuffle(result.products);
			var j=0;
			while (j<3){
				var prod=result.products[j];
				$('#newsCont').append('<div class="saleDiv">\
					<div class="saleName">'+getRandomNewsText()+'</div>\
					<a href=./productDescription.html?id='+prod.id+'>\
					<img class="mySale" src="'+prod.imageUrl[0]+'" alt="Oferta" onerror="imgError(this);">\
					</a>\
					<p class="saleName">'+prod.name+' - $'+prod.price+'</p>\
					</div>');
				j++;
			}		
		}});
	}else{
		var filtro=(ans.account.gender=="M"?"Masculino":"Femenino");
		$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&filters=[{%22id%22:5,%22value%22:%22Oferta%22},{%22id%22:1,%22value%22:%22"+filtro+"%22}]&page_size=100", success: function(result){
			shuffle(result.products);
			var i=0;
			while (i<3){
				var prod=result.products[i];
				$('#saleCont').append('<div class="saleDiv">\
					<div class="saleName">'+getRandomSaleText()+'</div>\
					<a href=./productDescription.html?id='+prod.id+'>\
					<img class="mySale" src="'+prod.imageUrl[0]+'" alt="Oferta" onerror="imgError(this);">\
					</a>\
					<p class="saleName">'+prod.name+' - $'+prod.price+'</p>\
					</div>');
				i++;
			}		
		}});

		$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&filters=[{%22id%22:6,%22value%22:%22Nuevo%22},{%22id%22:1,%22value%22:%22"+filtro+"%22}]&page_size=100", success: function(result){
			shuffle(result.products);
			var j=0;
			while (j<3){
				var prod=result.products[j];
				$('#newsCont').append('<div class="saleDiv">\
					<div class="saleName">'+getRandomNewsText()+'</div>\
					<a href=./productDescription.html?id='+prod.id+'>\
					<img class="mySale" src="'+prod.imageUrl[0]+'" alt="Oferta" onerror="imgError(this);">\
					</a>\
					<p class="saleName">'+prod.name+' - $'+prod.price+'</p>\
					</div>');
				j++;
			}		
		}});

	}
}

function getRandomSaleText(){
	var options=["¡Rebaja!","Gran descuento","Promoción imperdible","¡No te lo pierdas!"];
	var maxSize=options.length;
	randomIndex = Math.floor(Math.random() * maxSize);
	return options[randomIndex];
}

function getRandomNewsText(){
	var options=["Lo último","Recién llegado","Nuevo","Importado"];
	var maxSize=options.length;
	randomIndex = Math.floor(Math.random() * maxSize);
	return options[randomIndex];
}


function shuffle(array) {
	var currentIndex = array.length, temporaryValue, randomIndex ;
	while (0 !== currentIndex) {

		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;
}