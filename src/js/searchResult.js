var wholeResult;
var filteredResult;
var lastResult;
var searchHist = {};
var first;
var first_filtered;
var filters = {};
filters.names = [];
filters.values = [];
var curPageNumber = 12;
var curPage=1;

$(document).ready(function(){
	var key = getUrlParameter('key'); 

	localStorage.removeItem("AllProducts");
	
	var sufix="";
	switch(getUrlParameter("value")){
		case "Femenino": sufix="-Mujer";
		break;
		case "Masculino": sufix="-Hombre";
		break;
		case "Infantil": sufix="-Niño";
		break;

	}

	if(isEmptyBreadCrumb()){
		clearBreadCrumb();
		addBreadCrumb(key+sufix ,"./searchResult.html"+window.location.search);
		loadBreadCrumb();
	}


	

	$('#search-key').append("<h2>Resultados para: "+key+"</h2>");
	search(key);	


	

	/*$(".checkbox").change(function() {
		alert("changed");
		if(this.checked) {
		}
	});*/

$('.slider').slider();
$('.slider-horizontal').removeAttr('style');
});

function search(key){
	if((wholeResult==undefined) && ((typeof(Storage) == undefined) || (localStorage.getItem("AllProducts")===null))){
		first = true;
		first_filtered = true;
		$('#load-results-container').removeClass("tmp-container");
		getProductsPage(1, key);
	}else{
		if(wholeResult==undefined){
			wholeResult = JSON.parse(localStorage.getItem("AllProducts")).result;
		}
		first_filtered = true;
		if(key==undefined || key==""){
			$.each(wholeResult.products,function(){
				loadProduct(this);
			});
			localStorage.setItem("LastSearch", localStorage.getItem("AllProducts"));

			//aca vengo yo

		}else{
			$.each(wholeResult.products,function(){
				matchResult(key,this);
			});
			searchHist.time = new Date().getTime();		
			searchHist.result = filteredResult;
			localStorage.setItem("LastSearch", JSON.stringify(searchHist));
		}
		//alert(getUrlParameter("filter"));
		if(getUrlParameter("filter")==-1){
			removeLoading();
			initializeFilters();
			show();
		}
		else{
			initializeFilters(getUrlParameter("filter"), getUrlParameter("value"));
			removeLoading();
			show();
		}
		//alert(filteredResult.products.length);
	}
}

//Consulto de a páginas y genero HTML
function getProductsPage(pageNum , searchKey){


	var successLastRequest=true;
	var page=1;
	var PAGE_SIZE=60;
	successLastRequest = false;
	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetAllProducts&page_size="+PAGE_SIZE+"&page="+pageNum, success: function(result){
		if(first){
			first = false;
			wholeResult = {};
			wholeResult.products = [];
		}
		
		if(searchKey==undefined || searchKey==""){
			$.each(result.products,function(){
				wholeResult.products.push(this);
				successLastRequest = true;
				loadProduct(this);
			});
		}else{
			$.each(result.products,function(){
				wholeResult.products.push(this);
				successLastRequest = true;
				matchResult(searchKey,this);
			});
		}
		if(successLastRequest){
			getProductsPage(pageNum+1,searchKey);
			//alert("success");
		}else{
			//alert("fail");
			if(typeof(Storage) !== undefined) {
				var time_now = new Date().getTime();
				searchHist.time = time_now;		
				searchHist.result = wholeResult;
				localStorage.setItem("AllProducts", JSON.stringify(searchHist));
				searchHist.result = filteredResult;
				localStorage.setItem("LastSearch", JSON.stringify(searchHist));
			}
			removeLoading();
			initializeFilters();
			show();
			/*
				aca vengo yo :)
*/
//alert("asd");




}
}});
return;
};

function removeLoading(){
	$("#load-results-container").fadeOut("slow");
	$("#search-results-container").delay(800).slideDown("slow");

}

function matchResult(key,prod){

	key=key.toUpperCase().trim();
	prodname=prod.name.toUpperCase().trim();
	brandname=prod.attributes[0].values[0].toUpperCase().trim();
	categoryname=prod.category.name.toUpperCase().trim();
	subcategoryname=prod.subcategory.name.toUpperCase().trim();

	var printed=false;
	key.split("+").forEach(function(keyStr){
		prodname.split(" ").forEach(function (nameStr) {
			if(nameStr.indexOf(keyStr)==0 || keyStr.indexOf(nameStr)==0 ){
				loadProduct(prod);
				printed=true;
				return;
			}
		});

		if(!printed){
			brandname.split(" ").forEach(function (brandStr) {
				if(brandStr.indexOf(keyStr)==0 || keyStr.indexOf(brandStr)==0 ){
					loadProduct(prod);
					printed=true;
					return;
				}
			});
		}

		if(!printed){
			if(categoryname.indexOf(keyStr)==0 || keyStr.indexOf(categoryname)==0 ){
				loadProduct(prod);
				printed=true;
				return;
			}
		}			

		if(!printed){
			if(subcategoryname.indexOf(keyStr)==0 || keyStr.indexOf(subcategoryname)==0 ){
				loadProduct(prod);
				printed=true;
				return;
			}
		}
		

	});

	return;
}	

function loadProduct(prod){

	if(first_filtered){
		first_filtered = false;
		filteredResult = {};
		filteredResult.products = [];
	}
	filteredResult.products.push(prod);
	//addProduct(prod);
}

function addProduct(prod){
	var stringNode=createProductNode(prod.name,prod.price, prod.imageUrl, prod.attributes[0].values[0],prod.id);
	$("#search-results-container").append(stringNode);
}

function createProductNode(name,price,imageUrl,marca,id){
	var node= " <div class=\"search-result\">\
	<a href=\"./productDescription.html?id="+id+"\">\
	<img src=\""+imageUrl[0]+"\" alt=\""+name+"\" class=\"search-result-picture\" onerror=\"imgError(this);\">\
	</a>\
	<div>\
	<span class=\"result-field\">Nombre:</span><span class=\"result-value\">"+ name+"</span>\
	</div>\
	<div>\
	<span class=\"result-field\">Marca:</span><span class=\"result-value\">"+ marca+"</span>\
	</div>\
	<div>\
	<span class=\"result-field\">Precio:</span><span class=\"result-value\">$"+ price +"</span>\
	</div>\
	</div>\
	";
	return node;
};

function filter(param, value){
	filteredResult = JSON.parse(localStorage.getItem("filteredResult")).result;
	$.each(filteredResult.products, function(){
		filterAndLoad(param, value, this);
	});
}

function imgError(image) {
	image.onerror = "";
	image.src = "./img/noimage.png";
	return true;
}

function filterAndLoad(param, value, prod){
	var flag = false;
	$.each(prod.attributes, function(){
		if(this.name==param){
			$.each(this.values, function(){
				if(this==value)
					flag = true;
				return false;
			});
		}
	});
	//if(flag)
		//addProduct(prod);
	}

	function initializeFilters(filter_GET, value_GET){
		var filteredAttributes = [];
		var wholeAttributes = [];
		//alert(filter_GET + " " + value_GET);
		var flag = false;
		var attrNames = [];
		var addedAttrNames = [];

		$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Common.groovy?method=GetAllAttributes", success: function(result){
			var attrCount = result.attributes.length;
			$.each(result.attributes, function(){
				if($.inArray(this.name, attrNames) > -1)
					return true;
				attrNames.push(this.name);
				$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Common.groovy?method=GetAttributeById&id=" + this.id, 
					success: function(attribute){
						if(attribute != undefined && filteredResult!=undefined){
							wholeAttributes.push(attribute.attribute);
							$.each(filteredResult.products, function(j, prodX){
								if($.inArray(attribute.attribute.name, addedAttrNames) > -1)
									return false;
								$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductById&id=" + prodX.id, success: function(result2){
									if(result2.product != undefined && result2.product.attributes!=undefined)
										$.each(result2.product.attributes, function(k, att2){
											if(attribute.attribute.name == att2.name && $.inArray(attribute.attribute.name, addedAttrNames) == -1){
												addedAttrNames.push(attribute.attribute.name);
												createFilterGroupNode(attribute.attribute.name);
												$.each(attribute.attribute.values, function(l, value){
													createFilterNode(value, attribute.attribute.name);
												});
												if(filter_GET !=undefined && filter_GET == att2.name){
													alert(filter_GET+"-"+value_GET+"-checkbox");
													$("#"+filter_GET+"-"+value_GET+"-checkbox").prop('checked', true);
													updateFilter(filter_GET, value_GET, 1);												
												}	
												return false;
											}
										});
									show();
								}});
});
}
}});
});
}});
}

function createFilterGroupNode(value){
	var node = 
	'<div class="panel-group" id="accordion' + value + '" role="tablist" aria-multiselectable="true">\
	<div class="panel panel-default">\
	<a role="button" data-toggle="collapse" data-parent="#accordion' + value + '" href="#contenido-filtro-'+ value +'" aria-expanded="true" aria-controls="contenido-filtro-' + value + '">\
	<div class="panel-heading" role="tab" id="titulo-filtro-'+ value +'">\
	<h4 class="panel-title">\
	<span class="filter-title">' + value + '</span>\
	</h4>\
	</div>\
	</a>\
	<div id="contenido-filtro-' + value + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="titulo-filtro-' + value + '">\
	<div id="collapseListGroup1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="contenido-filtro-' + value + '" aria-expanded="true">\
	<ul class="list-group filter-list" id="filter-' + value + '">\
	</ul>\
	</div>\
	</div>\
	</div>\
	</div>';
	$(".filter-container").append(node);
}

function createFilterNode(value, filter){
	var node =
	'<li class="list-group-item">\
	<div class="checkbox">\
	<label><input id="' + filter + '-' + value + '-checkbox" type="checkbox" class="checkbox filter-checkbox" name="' + filter + '"onchange="changeFilter(this)" value="' + value + '">' + value + '</label>\
	</div>\
	</li>';
	$('#filter-'+ filter).append(node);
}

function changeFilter(check){
	var type;
	if(check.checked){
		type = 1;
	}else{
		type = 0;
	}
	updateFilter(check.name, check.value, type);
}

function updateFilter(name, value, type){
	if(type == 1){
		filters.names.push(name);
		filters.values.push(value);
		applyNewFilter(value, name);
	}else{
		var index = filters.names.indexOf(name);
		if(index != -1){
			filters.names.splice(index, 1);
			filters.values.splice(index, 1);
		}
		applyFilters();	
	}
}

function applyNewFilter(value, filter){
	if(lastResult == undefined)
		lastResult = JSON.parse(JSON.stringify(filteredResult));
	var aux = {};
	aux.products = [];
	
	clearProducts();

	$.each(lastResult.products, function(k, prod){		
		$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductById&id=" + prod.id, success: function(result){
			if(result.error != undefined)
				alert("error "+JSON.stringify(result.error));
			$.each(result.product.attributes, function(i, att){
				if(att.name == filter){
					$.each(att.values, function(j, val){
						if(val == value){
							aux.products.push(prod);
							//addProduct(prod);
							return false;
						}
					});
					return false;
				}
			});
		}});
	});
	lastResult.products = aux.products;
}

function applyFilters(){
	var aux = {};
	aux.products = [];
	
	clearProducts();
	//alert("size: " + filters.names.length);

	$.each(filteredResult.products, function(k, prod){		
		$.each(filters.names, function(l, filter){
			$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductById&id=" + prod.id, success: function(result){
				if(result.error != undefined)
					alert("error");
				$.each(result.product.attributes, function(i, att){
					//alert(att.name + " vs " + filter);
					if(att.name == filter){
						$.each(att.values, function(j, val){
							if(val == filters.values[l]){
								aux.products.push(prod);
								//addProduct(prod);
								return false;
							}
						});
						return false;
					}
				});
			}});
		});
	});
	lastResult.products = JSON.parse(JSON.stringify(aux.products));
}

function clearProducts(){
	$("#search-results-container").empty();
}



function displayProducts(){
	
	var workingWith;
	if(lastResult == undefined || lastResult.products.length==0){
		workingWith = JSON.parse(JSON.stringify(filteredResult));
	}else{
		var workingWith=JSON.parse(JSON.stringify(lastResult));
	}	
	
	var pageCount= Math.ceil(workingWith.products.length / curPageNumber);
	/*var cont = 1;
	while(cont<=pageCount){
		$('#search-pagination-bottom ul').append(createPageNode(cont));
		cont++;
	}*/
	pageLoader(pageCount);
	var prodCont= (curPage - 1 ) * curPageNumber;
	var limit= curPage * curPageNumber;
	//alert("DESDE:  "+prodCont+"   HASTA:   "+limit+"   O   "+workingWith.products.length)
	while(prodCont<workingWith.products.length && prodCont<limit){
		addProduct(workingWith.products[prodCont]);
		prodCont++;
	}
	
}

function show(){
	clearProducts();
	displayProducts();
}

function createPageNode(num){
	var node='<li ><a class="pNum">'+num+'</a></li>';
	return node;
}

function pageLoader(pageCount){

	$('#search-pagination-top ul').empty();
	$('#search-pagination-bottom ul').empty();
	var cont = 1;
	if(curPage!=1){
		$('#search-pagination-top ul').append(createPreviousNode());
		$('#search-pagination-bottom ul').append(createPreviousNode());
	}
	while(cont<=pageCount){
		$('#search-pagination-top ul').append(createPageNode(cont));
		$('#search-pagination-bottom ul').append(createPageNode(cont));
		cont++;
	}
	if(curPage<pageCount){
		$('#search-pagination-top ul').append(createNextNode());
		$('#search-pagination-bottom ul').append(createNextNode());
	}
	$('.pNum').click(function(){
		curPage= $(this).text();
		show();
	});

	$('.next').click(function(){
		curPage= curPage +1 ;
		show();
	})

	$('.prev').click(function(){
		curPage= curPage -1 ;
		show();
	})

}


function createPreviousNode(){
	var node='<li><a class="prev" href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
	return node;
}

function createNextNode(){
	var node='<li><a class="next" href="#" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
	return node;
}