$(document).ready(function(){

	clearBreadCrumb();
	addBreadCrumb("Mi carrito","./cart.html");
	loadBreadCrumb();

	var savedData=localStorage.getItem("accountData");
	var ans;
	if(savedData!=null){
		ans=JSON.parse(savedData);
	}else{
		savedData=sessionStorage.getItem("accountData");
		if(savedData!=null){
			ans=JSON.parse(savedData);
		}
	}
	
	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(result){
		if(result.error==undefined){

			if(result!=undefined && result.preferences!=null && result.preferences!=undefined)
				preferences=JSON.parse(result.preferences.replace( /\\/g ,''));
			if(result==undefined || result.preferences==undefined|| preferences.wishId==undefined || result.preferences==undefined){
				$(".not-logged-message").slideDown();
				$("#load-container").slideUp("slow");
				$(".total-container").slideUp("slow");
			}else
			$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+preferences.cartId, success: function(res){
				if(res.order.items.length == 0){
					$(".total-container").slideUp();
					$('#product_list').append('<div class="empty-cart">No tienes elementos en tu carrito por el momento.</div>');
					$("#load-container").fadeOut("slow");
					$("#product_list").delay(800).fadeIn("slow");
					$("#product_list").removeClass("tmp-container");
				}else{
					var total_price=0;
					var itemCont=0;
					$.each(res.order.items, function(){
						var tmp=JSON.parse(result.preferences.replace( /\\/g ,''))['item'+this.id];

						var pref_color="-";
						var pref_size="-";

						if(tmp!=undefined){
							pref_color=( tmp.color == undefined)?"-":tmp.color;
							pref_size=( tmp.size == undefined)?"-":tmp.size;
						}

						total_price+=this.price*this.quantity;
						$('#product_list').append(
							'<div class="row cart-row" id="product'+itemCont+'" name="id'+this.id+'" ">\
							<div class="col-md-3 cart-row-elem cart-img-col">\
							<a href="./productDescription.html?id='+this.product.id+'">\
							<img class="cart-row-img" src="'+this.product.imageUrl+'"" alt="'+this.name+'">\
							</a>\
							</div>\
							<div class="col-md-2 cart-row-elem">'+this.product.name+'</div>\
							<div class="col-md-2 cart-row-elem">\
							<div id="size-selector">'+pref_color+'</div>\
							</div>\
							<div class="col-md-1 cart-row-elem">\
							<div id="size-selector">'+pref_size+'</div>\
							</div>\
							<div class="col-md-1 cart-row-elem align-right">'+this.quantity+'\
							</div>\
							<div class="col-md-2 cart-row-elem align-right">$'+this.price+'</div>\
							<div class="col-md-1 red-cross-elem">\
							<button id="dispose'+itemCont+'" type="button" class="disposeButton close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="remove_elem">&times;</span></button>\
							</div>\
							</div>'
							);
itemCont++;
});
$('.total_price').append('$'+total_price);
$("#load-container").fadeOut("slow");
$("#product_list").delay(800).slideDown("slow");
$(".total-container").delay(800).slideDown("slow");
}
}, complete:function(){

	$(".disposeButton").click(function () {
		var product= $("#product"+this.id.replace(/^dispose/,''));
		$(this).slideUp();
		$(this).parent().append('<span class="textToAppear">Borrando...</span>');
		$(".textToAppear").slideDown();
		$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=RemoveItemFromOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+product.attr('name').replace(/^id/,''), success: function(res){
			delete preferences["item"+product.attr('name').replace(/^id/,'')];
			$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdatePreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&value='+JSON.stringify(preferences), success: function(newPref){
				product.slideUp();
				var prevCant= parseInt($("#badge-cart-logged").text());
				$("#badge-cart-logged").empty();
				$("#badge-cart-logged").append(prevCant-1);

				if(prevCant==1){
					$(".total-container").slideUp();
					$('#product_list').append('<div class="empty-cart">No tienes elementos en tu carrito por el momento.</div>');
					$("#product_list").delay(800).fadeIn("slow");
				}

			}});
		}});
	});
}});
}else{
	displayApiError(result.error.code,result.error.message);
}
}});
})
