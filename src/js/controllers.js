var registerApp = angular.module('registerApp', ['ngMessages']);
//var loginApp = angular.module('loginApp', []);

registerApp.controller('validator', function($scope){});

registerApp.controller('signinvalidator', function($scope){});

registerApp.directive('alphanumericCheck', alphanumeric);

registerApp.directive('passwordMatch', function (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			
			ngModel.$parsers.unshift(function(value) {
				if(value === $('#pwdreg').val())
					valid = true;
				else
					valid = false
				ngModel.$setValidity('passwordMatch', valid);
				return valid ? value : undefined;
			});

			ngModel.$formatters.unshift(function(value) {
				if(value === $('#pwdreg').val())
					valid = true;
				else
					valid = false
				ngModel.$setValidity('passwordMatch', valid);
				return valid ? value : undefined;
			});
		}
	};
});


registerApp.directive('alphanumericAndSpecialsCheck', alphanumericAndSpecials);

registerApp.directive('checkedCheck', function (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function(value) {
				ngModel.$setValidity('checkedCheck', value);
				return valid ? value : undefined;
			});
		}
	};
});

registerApp.directive('documentFormat', function (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			
			ngModel.$parsers.unshift(function(value) {
				var valid = /^[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]$/.test(value);
				ngModel.$setValidity('documentFormat', valid);
				return valid ? value : undefined;
			});
		}
	};
});

registerApp.directive('ageCheck', function (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function(value){
				var valid;
				if((new Date().getFullYear() - value) < 18)
					valid = false;
				else
					valid = true;
				ngModel.$setValidity('ageCheck', valid);
				return valid ? value : undefined;
			});
		}
	};
});

//////////////////////////PROFILEAPP/////////////////////////////////////////////////////

var profileApp = angular.module('profileApp', ['ngMessages']);

profileApp.controller('addaddress', function($scope){});

profileApp.controller('repeat', function($scope){
	$scope.addresses=[];
	$scope.allStates=[];
});

profileApp.controller('changepass', function($scope){
});

profileApp.directive('passwordMatch', function (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			
			ngModel.$parsers.unshift(function(value) {
				if(value === $('#re_prev_pwd').val())
					valid = true;
				else
					valid = false
				ngModel.$setValidity('passwordMatch', valid);
				return valid ? value : undefined;
			});

			ngModel.$formatters.unshift(function(value) {
				if(value === $('#re_prev_pwd').val())
					valid = true;
				else
					valid = false
				ngModel.$setValidity('passwordMatch', valid);
				return valid ? value : undefined;
			});
		}
	};
});

profileApp.directive('checkMailMatch', function (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			
			ngModel.$parsers.unshift(function(value) {
				if(value === $('#new_email').val())
					valid = true;
				else
					valid = false
				ngModel.$setValidity('checkMailMatch', valid);
				return valid ? value : undefined;
			});

			ngModel.$formatters.unshift(function(value) {
				if(value === $('#new_email').val())
					valid = true;
				else
					valid = false
				ngModel.$setValidity('checkMailMatch', valid);
				return valid ? value : undefined;
			});
		}
	};
});

//profileApp.directive('provinceHandler', provinceHandler);

profileApp.directive('alphanumericAndSpecialsCheck', alphanumericAndSpecials);

profileApp.directive('provinceCheck', provinceCheck);

profileApp.directive('numericCheck', numeric);

profileApp.directive('alphanumericCheck', alphanumeric);



////////////////////////ADDRESSESAPP/////////////////////////////////////////////////////




//////////////////////FUNCTIONS/////////////////////

function alphanumericAndSpecials (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			
			ngModel.$parsers.unshift(function(value) {
				var valid = /^[a-zA-Z0-9\+\-\,\.\_\;\%\/\$\#\@]*$/.test(value);
				ngModel.$setValidity('alphanumericAndSpecialsCheck', valid);
				return valid ? value : undefined;
			});

		}
	};
}

function provinceCheck (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {

			ngModel.$parsers.unshift(function(value) {
				if(value!="Provincia")
					valid = true;
				else
					valid = false;
				ngModel.$setValidity('provinceCheck', valid);
				return valid ? value : undefined;
			});

		}
	};
}

function provinceCheck (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function(value) {
				if(value!="Provincia")
					valid = true;
				else
					valid = false;
				ngModel.$setValidity('provinceCheck', valid);
				return valid ? value : undefined;
			});

		}
	};
}

function numeric (){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {

			ngModel.$parsers.unshift(function(value) {
				var valid = /^[0-9]*$/.test(value);
				ngModel.$setValidity('numericCheck', valid);
				return valid ? value : undefined;
			});

		}
	};
}

function alphanumeric(){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function(value) {

				var valid = /^[a-zA-Z0-9]*$/.test(value);
				

				ngModel.$setValidity('alphanumericCheck', valid);
				return valid ? value : undefined;
			});

		}
	};
}

function usernameCheck(){ 
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			ngModel.$parsers.unshift(function(value) {

				var valid = /^[a-zA-Z0-9]*$/.test(value) && /^[a-zA-Z]*$/.test(value.substring(0,1));
				

				ngModel.$setValidity('alphanumericCheck', valid);
				return valid ? value : undefined;
			});

		}
	};
}