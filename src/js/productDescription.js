$(document).ready(function(){

	var prodId = getUrlParameter('id');

	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Catalog.groovy?method=GetProductById&id="+prodId, success: function(prod){

		if(prod!=undefined && prod.product!=undefined){
			$("#load-container").slideUp();
			$(".tmp-container").slideDown();


			$("#product-name").append(prod.product.name);
			addBreadCrumb(prod.product.name,prodId);
			var cont=0;

			$.each(prod.product.imageUrl,function(){
				var min_class="unselected-min-img";
				if(cont==0){
					min_class="selected-min-img";
				}
				var pictureNode="<li class=\"min-prod-item\"><img id=\"min_img_"+cont+"\" class=\"min-product-image "+min_class+"\" src=\""+this+"\" alt=\""+prod.product.name+"\" onerror=\"imgDeteleError(this);\"></li>";
				$("#min-pictures-container ul").append(pictureNode);
				cont++;
			});

			var mainImg="<img id=\"product-image\" src=\""+prod.product.imageUrl[0]+"\" alt=\""+prod.product.name+"\" onerror=\"imgError(this);\">";
			$("#product-picture-container").append(mainImg);

			$("#prod-brand").append(getValueFromJSON(prod.product.attributes,"Marca")[0]);	

			$("#prod-price").append("$"+prod.product.price);	

			$("#prod-mat").append(getValueFromJSON(prod.product.attributes,"Material")[0]);	

			$.each(getValueFromJSON(prod.product.attributes,"Talle"),function(){
				$("#size-selector").append("<option>"+this+"</option>");
			});

			$.each(getValueFromJSON(prod.product.attributes,"Color"),function(){
				$("#color-selector").append("<option>"+this+"</option>");
			});
		}
	}});

var savedData=localStorage.getItem("accountData");
if(savedData!=null){
	var ans=JSON.parse(savedData);
}else{
	savedData=sessionStorage.getItem("accountData");
	if(savedData!=null){
		var ans=JSON.parse(savedData);
	}
}

if(savedData==null){
	$(".button-options").empty();
	$(".button-options").append('<div class="not-logged-message"> Para poder realizar compras debe ser un usuario registrado </div>');
}

$("#desc-cart").click(function(){
	var quant=$("#quant-selector").val();
	var color=$("#color-selector").val();
	var size=$("#size-selector").val();

	$("#desc-cart").empty();
	$("#desc-cart").append("Agregando al carrito.");
	$("#desc-cart").prop('disabled',true);
	

	var savedData=localStorage.getItem("accountData");
	if(savedData!=null){
		var ans=JSON.parse(savedData);
	}else{
		savedData=sessionStorage.getItem("accountData");
		if(savedData!=null){
			var ans=JSON.parse(savedData);
		}
	}

	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(res){
		if(res!=undefined && res.preferences!=undefined){
			prefer=JSON.parse(res.preferences.replace( /\\/g ,''));
			if(prefer.cartId!=undefined){
				$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=AddItemToOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&order_item={"order":{"id":'+prefer.cartId+'},"product":{"id": '+prodId+'},"quantity":'+quant+'}', success: function(resp){
					var newElem={
						"color":color,
						"size":size,
					};
					prefer["item"+resp.orderItem.id]=newElem;
					$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdatePreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&value='+JSON.stringify(prefer), success: function(newPref){
						$("#message_div_cart").slideUp();
						$("#message_div_cart").empty();
						$("#message_div_cart").append("Se agregó correctamente al carrito.");
						$("#message_div_cart").slideDown();
						$("#desc-cart").empty();
						$("#desc-cart").append("Agregar al carrito.");
						$("#desc-cart").prop('disabled',false);

						var prevCant= parseInt($("#badge-cart-logged").text());
						$("#badge-cart-logged").empty();
						$("#badge-cart-logged").append(prevCant+1);
					}});
				}});
}else{
	$(".button-options").empty();
	$(".button-options").append('<div class="not-logged-message"> La cuenta registrada no fue creada en esta página. No dispone de carrito ni lista de deseos. </div>');

}
}else{
	$(".button-options").empty();
	$(".button-options").append('<div class="not-logged-message"> La cuenta registrada no fue creada en esta página. No dispone de carrito ni lista de deseos. </div>');
}
}});

})

$("#desc-wish").click(function(){
	var quant=$("#quant-selector").val();
	var color=$("#color-selector").val();
	var size=$("#size-selector").val();


	
	$("#desc-wish").empty();
	$("#desc-wish").append("Agregando a lista de deseos.");
	$("#desc-wish").prop('disabled',true);


	var savedData=localStorage.getItem("accountData");
	if(savedData!=null){
		var ans=JSON.parse(savedData);
	}else{
		savedData=sessionStorage.getItem("accountData");
		if(savedData!=null){
			var ans=JSON.parse(savedData);
		}
	}

	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(res){
		if(res!=undefined && res.preferences!=undefined){
			prefer=JSON.parse(res.preferences.replace( /\\/g ,''));
			if(prefer.cartId!=undefined){
				$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=AddItemToOrder&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+'&order_item={"order":{"id":'+prefer.wishId+'},"product":{"id": '+prodId+'},"quantity":'+quant+'}', success: function(resp){
					$("#message_div_wish").slideUp();
					$("#message_div_wish").empty();
					$("#message_div_wish").slideDown();
					$("#message_div_wish").append("Se agregó correctamente a la lista de deseos.");
					$("#desc-wish").empty();
					$("#desc-wish").append("Agregar a la lista de deseos.");
					$("#desc-wish").prop('disabled',false);

					var prevCant= parseInt($("#badge-wish-logged").text());
					$("#badge-wish-logged").empty();
					$("#badge-wish-logged").append(prevCant+1);
				}});
			}
		}else{
			$(".button-options").empty();
			$(".button-options").append('<div class="not-logged-message"> La cuenta registrada no fue creada en esta página. No dispone de carrito ni lista de deseos. </div>');
		}
	}});

})


$(document).on("click", ".min-product-image", function(){
	var idclass=this.id;
	
	$(".selected-min-img").addClass("unselected-min-img");
	$(".selected-min-img").removeClass("selected-min-img");
	
	$("#"+idclass).addClass("selected-min-img");
	$("#"+idclass).removeClass("unselected-min-img");

	$("#product-image").attr("src",this.src);

});

});

function addBreadCrumb(prodName,prodId){

	var storageItem=JSON.parse(sessionStorage.getItem("breadcrumbs"));
	if(storageItem != null){
		var newBread=[];
		for(i=0;i<2;i++){
			if(storageItem[i]!=undefined){
				newBread.push(storageItem[i]);
			}
		}
		newBread.push({"title":prodName,"bcUrl":"./productDescription.html?id="+prodId});	
		sessionStorage.setItem("breadcrumbs",JSON.stringify(newBread));
		loadBreadCrumb();
	}
}