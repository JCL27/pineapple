var allStates = [];

$(document).ready(function(){
	
	clearBreadCrumb();
	addBreadCrumb("Mi Perfil","./userConfig.html");
	loadBreadCrumb();

	var savedData = JSON.parse(localStorage.getItem("accountData"));
	if(savedData==null){
		savedData = JSON.parse(sessionStorage.getItem("accountData"));
	}
	$("#accName").append(savedData.account.firstName);

	//$("#accUname").append(savedData.account.username);
	//alert("USER_NAME: " + savedData.account.username + "  AUT_TOKEN: " + savedData.authenticationToken);

	loadOrders(savedData);
	$("#newAddress-button").click(createAddress);

	$.ajax({url: "http://eiffel.itba.edu.ar/hci/service3/Common.groovy?method=GetAllStates", success: function(result){
		var scope = angular.element($("#Addresses")).scope();
		if(scope!=undefined)
			scope.$apply(function(){
				scope.allStates = JSON.parse(JSON.stringify(result.states));
			});
		loadAddresses(savedData);
	}});

	/*
	$.each(result.states, function(){
		sessionStorage.setItem("state-name:" + this.name,JSON.stringify(this.stateId));
	});*/

if(savedData == undefined || savedData == null) 
	window.location.href = "./mainPage.html";

$("#accUname").append((savedData.account.username.charCodeAt(0)=='0'.charCodeAt(0))?"-":savedData.account.username);
$("#accIdentity").append(savedData.account.identityCard);
var aux=savedData.account.birthDate.split('-');;
$("#accBdate").append(aux[2]+"/"+aux[1]+"/"+aux[0]);
$("#accSurname").append(savedData.account.lastName);
$("#accMail").append(savedData.account.email);
$("#accGen").append(savedData.account.gender);

$("#submit_chpass").click(function () {
	var savedData=JSON.parse(localStorage.getItem("accountData"));
	if(savedData==null){
		savedData=JSON.parse(sessionStorage.getItem("accountData"));
	}

	var pass=$("#prev_pwd").val();
	var repass=$("#re_prev_pwd").val();
	var newpass=$("#new_pwd").val();

	if(repass==newpass){
		$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=ChangePassword&username="+savedData.account.username+"&password="+pass+"&new_password="+newpass, success: function(result){
		}});
	}else{
		alert("No coinciden. Esto va por angular");
	}
});

$("#submit_chemail").click(function () {
	var savedData=JSON.parse(localStorage.getItem("accountData"));
	if(savedData==null){
		savedData=JSON.parse(sessionStorage.getItem("accountData"));
	}
	var newMail=$("#new_email").val();
	var reNewMail=$("#re_new_email").val();

	var regis=savedData.account;

	if(regis.username.charCodeAt(0)=='0'.charCodeAt(0)){
		regis.username=hashMail(newMail);
	}

	if(newMail==reNewMail){
		var request="http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateAccount&";
		request += 'username='+ savedData.account.username +'&authentication_token='+savedData.authenticationToken+'&account={';
		request += '"username":"' + regis.username + '",';
		request += '"password":"' + regis.pwdreg + '",';
		request += '"firstName":"' + regis.firstName + '",';
		request += '"lastName":"'+ regis.lastName + '",';
		request += '"gender":"' + regis.gender + '",';
		request += '"identityCard":"' + regis.identityCard + '",';
		request += '"email":"'+ newMail + '",';
		request += '"birthDate":"' + regis.birthDate + '"';
		request += "}";
			//alert(request);

			$.ajax({url:request, success: function(result){
				$("#accMail").empty(newMail);
				$("#accMail").append(newMail);
				$('#change-email').modal('toggle');

				savedData.account.email=newMail;

				var newaux=JSON.parse(localStorage.getItem("accountData"));
				if(newaux!=null){
					localStorage.removeItem("accountData");
					localStorage.setItem("accountData",JSON.stringify(savedData));
				}else{
					sessionStorage.removeItem("accountData");
					sessionStorage.setItem("accountData",JSON.stringify(savedData));
				}

			}});
		}else{
			alert("No coinciden. Esto va por angular");
		}
	});
})


function loadAddresses(savedData){

	var request = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetAllAddresses';
	request += '&username=' + savedData.account.username;
	request += '&authentication_token=' + savedData.authenticationToken;

	$.ajax({url: request, success: function(result){
		if(result.error!=undefined){
			alert("ERROR CODE: "+ result.error.code);
		}
		$.each(result.addresses, function(){
			sessionStorage.setItem("address-id-" + this.id, JSON.stringify(this));
			showAddress(this);
		});

	}, complete:function(result){
		$(".disposeButton").click(deleteAddress);
	}});

}

function showAddress(address){
	
	/* Optionals */
	var scope = angular.element($("#Addresses")).scope();
	if(scope!=undefined)
		scope.$apply(function(){
			scope.addresses.push(address);
		});

	if(address.floor==undefined){
		var floorOption = 'placeholder="-"';
	} else{
		var floorOption = "value = " + address.floor;
	}
	if(address.city==undefined){
		var cityOption = 'placeholder="-"';
	} else{
		var cityOption = "value = " + address.city;
	}
	
	var node = 
	'<div class="panel-footer" id="' + address.id + '" >\
	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+address.id+'" aria-expanded="true" aria-controls="collapse'+address.id+'">\
	' + address.name + '\
	</a>\
	<button type="button" class="close disposeButton" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
	</div>\
	<div id="collapse'+address.id+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="' + address.id +'" >\
	<div class="panel-body">\
	<div class="city-picker">\
	<div class="form-group">\
	<label>Provincia</label>\
	<select class="enabledSelect form-control">\
	{{address.province}}\
	<option ng-repeat="state in allStates" ng-attr-selected="{{"'+address.province+'"==state.stateId ? true : false}}" value="{{state.stateId}}">{{state.name}}</option>\
	</select>\
	</div>\
	<div class="form-group">\
	<label for="cityInput">Ciudad</label>\
	<input type="text" class="form-control floor-input" onfocusout="cityHandler(this)" '+ cityOption +' id="cityInput">\
	</div>\
	</div>\
	<div class="adress-picker">\
	<div class="form-group">\
	<label for="streetInput">Calle</label>\
	<input type="text" class="form-control" id="streetInput" onfocusout="streetHandler(this)" value="' + address.street +'">\
	</div>\
	<div class="form-group number-box">\
	<label for="numberInput">Número</label>\
	<input type="text" class="form-control" id="numberInput" onfocusout="numberHandler(this)" value="' + address.number + '">\
	</div>\
	<div class="form-group">\
	<label for="floorInput">Departamento</label>\
	<input type="text" class="form-control floor-input" onfocusout="floorHandler(this)" '+ floorOption +' id="floorInput" ">\
	</div>\
	<div class="form-group number-box">\
	<label for="zipcodeInput">Código Postal</label>\
	<input type="text" class="form-control" id="zipcodeInput" onfocusout="zipcodeHandler(this)" value="' + address.zipCode +'">\
	</div>\
	<div class="form-group">\
	<label for="phonenumberInput">Número de teléfono</label>\
	<input type="text" class="form-control" id="phonenumberInput" onfocusout="phonenumberHandler(this)" value="' + address.phoneNumber + '">\
	</div>\
	</div>\
	</div>\
	</div>';

	//$("#Addresses").append(node);
}


function createAddress(){ 
	var address = {};

	address.name = $('#addressName').val();
	address.street = $('#street').val();
	address.number = $('#number').val();
	address.province = $('#province').val();
	address.city = $('#city').val();
	address.floor = $('#floor').val();
	address.zipCode = $('#zipCode').val();
	address.phoneNumber = $('#phoneNumber').val();	

	var savedData = JSON.parse(localStorage.getItem("accountData"));
	if(savedData==null){
		savedData = JSON.parse(sessionStorage.getItem("accountData"));
	}

	if(address.province!='Ciudad Autonoma de Buenos Aires' && address.city==''){
		alert("NO SE PUEDE TENER CITY VACIO CON PROVINCE != 'C'")
	}

	var request = "http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=CreateAddress";
	request += '&username=' + savedData.account.username;
	request += '&authentication_token=' + savedData.authenticationToken;
	request += '&address={';
	request += '"name":"'+ address.name + '",';
	request += '"street":"'+ address.street + '",';
	request += '"number":"'+ address.number + '",';
	request += '"floor":"' + address.floor + '",';
	request += '"city":"'+ address.city + '",';
	request += '"province":"'+ address.province + '",';
	request += '"zipCode":"'+ address.zipCode + '",';
	request += '"phoneNumber":"'+ address.phoneNumber + '"';
	request += "}";

	$.ajax({url: request, success: function(result){
		if(result.error==undefined){
			showAddress(result.address);
			sessionStorage.setItem("address-id-" + result.address.id, JSON.stringify(result.address));
		} else {
			alert("ERROR CODE: "+ result.error.code);
		}
	}});


	$('#newAddress').modal('hide');
	$(".disposeButton").click(deleteAddress);
}


function cityHandler(element){
	
	var addressId = $(element).parent().parent().parent().parent().prev().attr('id');
	var savedData = JSON.parse(localStorage.getItem("accountData"));
	if(savedData==null){
		savedData = JSON.parse(sessionStorage.getItem("accountData"));
	}

	var address = JSON.parse(sessionStorage.getItem("address-id-" + addressId));
	if($(element).val()=="" && address.province!='Ciudad Autonoma de Buenos Aires'){
		alert("NO SE PUEDE TENER CITY VACIO CON PROVINCIA !='Ciudad Autonoma de Buenos Aires'");
	} else{
		address.city = $(element).val();
		if(address.floor==null){
			address.floor = undefined;
		}
		address.gate = undefined;
		var updateRequest = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateAddress';
		updateRequest += '&username=' + savedData.account.username;
		updateRequest += '&authentication_token=' + savedData.authenticationToken;
		updateRequest += '&address=' + JSON.stringify(address).replace(/\\/g,'');

		$.ajax({url: updateRequest, success: function(result){
			if(result.error==undefined){
				alert("La direccion se ha modificado exitosamente.")
			} else {
				alert("ERROR CODE: "+ result.error.code);
			}
		}});
	}
}

function streetHandler(element){
	if($(element).val()==""){
		alert("NO SE PUEDE TENER UNA CALLE VACIA");
	}else {
		var addressId = $(element).parent().parent().parent().parent().prev().attr('id');
		var savedData = JSON.parse(localStorage.getItem("accountData"));
		if(savedData==null){
			savedData = JSON.parse(sessionStorage.getItem("accountData"));
		}

		var address = JSON.parse(sessionStorage.getItem("address-id-" + addressId));
		address.street = $(element).val();
		if(address.floor==null){
			address.floor = undefined;
		}
		address.gate = undefined;

		var updateRequest = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateAddress';
		updateRequest += '&username=' + savedData.account.username;
		updateRequest += '&authentication_token=' + savedData.authenticationToken;
		updateRequest += '&address=' + JSON.stringify(address).replace(/\\/g,'');

		$.ajax({url: updateRequest, success: function(result){
			if(result.error==undefined){
				alert("La direccion se ha modificado exitosamente.")
			} else {
				alert("ERROR CODE: "+ result.error.code);
			}
		}});
	}
}


function numberHandler(element){
	if($(element).val()==""){
		alert("NO SE PUEDE TENER UN NUMERO VACIO");
	}else {
		var addressId = $(element).parent().parent().parent().parent().prev().attr('id');
		var savedData = JSON.parse(localStorage.getItem("accountData"));
		if(savedData==null){
			savedData = JSON.parse(sessionStorage.getItem("accountData"));
		}

		var address = JSON.parse(sessionStorage.getItem("address-id-" + addressId));
		address.number = $(element).val();
		if(address.floor==null){
			address.floor = undefined;
		}
		address.gate = undefined;
		var updateRequest = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateAddress';
		updateRequest += '&username=' + savedData.account.username;
		updateRequest += '&authentication_token=' + savedData.authenticationToken;
		updateRequest += '&address=' + JSON.stringify(address).replace(/\\/g,'');

		$.ajax({url: updateRequest, success: function(result){
			if(result.error==undefined){
				alert("La direccion se ha modificado exitosamente.")
			} else {
				alert("ERROR CODE: "+ result.error.code);
			}
		}});
	}
}


function floorHandler(element){
	var addressId = $(element).parent().parent().parent().parent().prev().attr('id');
	var savedData = JSON.parse(localStorage.getItem("accountData"));
	if(savedData==null){
		savedData = JSON.parse(sessionStorage.getItem("accountData"));
	}

	var address = JSON.parse(sessionStorage.getItem("address-id-" + addressId));
	address.floor = $(element).val();
	if($(element).val()==''){
		address.floor = undefined;
	}
	address.gate = undefined;
	var updateRequest = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateAddress';
	updateRequest += '&username=' + savedData.account.username;
	updateRequest += '&authentication_token=' + savedData.authenticationToken;
	updateRequest += '&address=' + JSON.stringify(address).replace(/\\/g,'');

	console.log("REQUEST: " + updateRequest);

	$.ajax({url: updateRequest, success: function(result){
		if(result.error==undefined){
			alert("La direccion se ha modificado exitosamente.")
		} else {
			alert("ERROR CODE: "+ result.error.code);
		}
	}});
}


function zipcodeHandler(element){
	if($(element).val()==""){
		alert("NO SE PUEDE TENER UN ZIPCODE VACIO");
	}else {
		var addressId = $(element).parent().parent().parent().parent().prev().attr('id');
		var savedData = JSON.parse(localStorage.getItem("accountData"));
		if(savedData==null){
			savedData = JSON.parse(sessionStorage.getItem("accountData"));
		}

		var address = JSON.parse(sessionStorage.getItem("address-id-" + addressId));
		address.zipCode = $(element).val();
		if(address.floor==null){
			address.floor = undefined;
		}
		address.gate = undefined;
		var updateRequest = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateAddress';
		updateRequest += '&username=' + savedData.account.username;
		updateRequest += '&authentication_token=' + savedData.authenticationToken;
		updateRequest += '&address=' + JSON.stringify(address).replace(/\\/g,'');

		$.ajax({url: updateRequest, success: function(result){
			if(result.error==undefined){
				alert("La direccion se ha modificado exitosamente.")
			} else {
				alert("ERROR CODE: "+ result.error.code);
			}
		}});
	}
}


function phonenumberHandler(element){
	if($(element).val()==""){
		alert("NO SE PUEDE TENER UN NUMERO TELEFONICO VACIO");
	}else {
		var addressId = $(element).parent().parent().parent().parent().prev().attr('id');
		var savedData = JSON.parse(localStorage.getItem("accountData"));
		if(savedData==null){
			savedData = JSON.parse(sessionStorage.getItem("accountData"));
		}

		var address = JSON.parse(sessionStorage.getItem("address-id-" + addressId));
		address.phoneNumber = $(element).val();
		if(address.floor==null){
			address.floor = undefined;
		}
		address.gate = undefined;
		var updateRequest = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=UpdateAddress';
		updateRequest += '&username=' + savedData.account.username;
		updateRequest += '&authentication_token=' + savedData.authenticationToken;
		updateRequest += '&address=' + JSON.stringify(address).replace(/\\/g,'');

		$.ajax({url: updateRequest, success: function(result){
			if(result.error==undefined){
				alert("La direccion se ha modificado exitosamente.")
			} else {
				alert("ERROR CODE: "+ result.error.code);
			}
		}});
	}
}

function deleteAddress(){
	var addressId = $(this).parent().attr('id');
	alert(addressId);
	var savedData = JSON.parse(localStorage.getItem("accountData"));
	if(savedData==null){
		savedData = JSON.parse(sessionStorage.getItem("accountData"));
	}

	var request = 'http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=DeleteAddress';
	request += '&username=' + savedData.account.username;
	request += '&authentication_token=' + savedData.authenticationToken;
	request += '&id=' + addressId;

	$.ajax({url: request, success: function(result){
		if(result.error==undefined){
			var scope = angular.element($("#Addresses")).scope();
			if(scope!=undefined)
				scope.$apply(function(){
					$.each(scope.addresses, function(i, addr){
						if(addr.id == addressId){
							scope.addresses.splice(i, 1);
							return false;
						}
					});	
				});
			$("#" + addressId).slideUp();
			$("#" + addressId).remove("address-id-" + addressId);
			sessionStorage.removeItem("address-id-" + addressId);
		} else {
			alert("ERROR CODE: "+ result.error.code);
		}
	}});
}

function loadOrders(savedData){

	$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+savedData.account.username+"&authentication_token="+savedData.authenticationToken, success: function(res){
		if(res!=null){
			if(res.preferences!=null)
				var preferences=JSON.parse(res.preferences.replace( /\\/g ,''));
			$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetAllOrders&username="+savedData.account.username+"&authentication_token="+savedData.authenticationToken, success: function(allOrders){
				$.each(allOrders.orders,function(){
					if((res.preferences==null) || (preferences.wishId==null && preferences.cartId==null) || this.id!=preferences.wishId && this.id!=preferences.cartId){
						var addressName="-";
						if(this.address!=undefined){
							addressName=this.address.name;
						}
						var orderState="-";
						switch(this.status){
							case "1": orderState="Pendiente";
							break;
							case "2": orderState="Confirmada";
							break;
							case "3": orderState="Transportada";
							break;
							case "4": orderState="Entregada";
							break;							
						}
						var fecha=this.receivedDate.split(" 00:00")[0].split("-");

						$("#prevOrdersBody").append('<tr>\
							<td>'+fecha[2]+"/"+fecha[1]+"/"+fecha[0]+'</td>\
							<td>'+addressName+'</td>\
							<td>'+orderState+'</td>\
							<td><button type="button" value="'+this.id+'" class="btn btn-success btn-circle view-product-modal-trigger"><i class="glyphicon glyphicon-plus"></i></button></td>\
							</tr>');
					}
				});

				$(".view-product-modal-trigger").click(function () {
					$('#product_list').empty();
					$('#load-container').slideDown();
					$("#product_list").slideUp("slow");
					$('#view-products-modal').modal('toggle');


					var orderId=$(this).attr('value');
					var savedData=localStorage.getItem("accountData");
					var ans;
					if(savedData!=null){
						ans=JSON.parse(savedData);
					}else{
						savedData=sessionStorage.getItem("accountData");
						if(savedData!=null){
							ans=JSON.parse(savedData);
						}
					}

					$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Account.groovy?method=GetPreferences&username="+ans.account.username+"&authentication_token="+ans.authenticationToken, success: function(result){
						if(result.preferences!=null)
							var preferences=JSON.parse(result.preferences.replace( /\\/g ,''));
						$.ajax({url:"http://eiffel.itba.edu.ar/hci/service3/Order.groovy?method=GetOrderById&username="+ans.account.username+"&authentication_token="+ans.authenticationToken+"&id="+orderId, success: function(res){
							if(res.order.items.length == 0){
								$(".total-container").slideUp();
								$('#product_list').append('<div class="empty-cart">La orden no tiene productos.</div>');
								$("#load-container").fadeOut("slow");
								$("#product_list").delay(800).fadeIn("slow");
								$("#product_list").removeClass("tmp-container");
							}else{
								var total_price=0;
								var itemCont=0;
								$.each(res.order.items, function(){
									if(result.preferences!=null)
										var tmp=JSON.parse(result.preferences.replace( /\\/g ,''))['item'+this.id];

									var pref_color="-";
									var pref_size="-";

									if(tmp!=undefined){
										pref_color=( tmp.color == undefined)?"-":tmp.color;
										pref_size=( tmp.size == undefined)?"-":tmp.size;
									}

									total_price+=this.price*this.quantity;
									$('#product_list').append(
										'<div class="row cart-row" id="product'+itemCont+'" name="id'+this.id+'" ">\
										<div class="col-md-3 cart-row-elem cart-img-col">\
										<a href="./productDescription.html?id='+this.product.id+'">\
										<img class="cart-row-img" src="'+this.product.imageUrl+'"" alt="'+this.name+'">\
										</a>\
										</div>\
										<div class="col-md-2 cart-row-elem">'+this.product.name+'</div>\
										<div class="col-md-2 cart-row-elem">\
										<div id="size-selector">'+pref_color+'</div>\
										</div>\
										<div class="col-md-1 cart-row-elem">\
										<div id="size-selector">'+pref_size+'</div>\
										</div>\
										<div class="col-md-1 cart-row-elem align-right">'+this.quantity+'\
										</div>\
										<div class="col-md-2 cart-row-elem">$'+this.price+'</div>\
										</div>');
									itemCont++;
								});
$(".total-container").slideDown();
$('.total_price').empty();
$('.total_price').append('Precio total: $'+total_price);
$('.total_price').slideDown();
$("#load-container").slideUp("slow");
$("#product_list").slideDown("slow");
$("#product_list").removeClass("tmp-container");
}

}});
}});



});
}});
}
}});
}

